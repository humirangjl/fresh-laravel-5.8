const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.webpackConfig(webpack => {
    return {
        plugins: [
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery',
                'window.jQuery': 'jquery',
                Popper: ['popper.js', 'default'],
            })
        ]
    };
});


mix.scripts([
        'resources/frontend/js/vendor/jquery-v3.3.1.min.js',
        'resources/frontend/js/main.js',
    ], 'public/frontend/js/app.js')
    .styles([
        'resources/frontend/css/bootstrap.min.css',
        'resources/frontend/css/font-awesome.min.css',
        'resources/frontend/css/main.css',
    ], 'public/frontend/css/app.css')
    .js('resources/js/app.js', 'public/backend/js')
    .sass('resources/sass/app.scss', 'public/backend/css')
    .copyDirectory('resources/static/images','public/images')
    .copyDirectory('resources/backend','public/backend')
    .browserSync('laradminator.local')
    .version()
    .sourceMaps();
