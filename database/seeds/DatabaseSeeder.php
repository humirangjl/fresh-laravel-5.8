<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            users::class,
            page_sections::class,
            configuration::class,
            DefaultSetting::class,
        ]);
    }
}
