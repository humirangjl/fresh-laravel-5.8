<li class="nav-item active">
    <a class='sidebar-link' href="{{ route(ADMIN . '.content-management.index') }}">
        <span class="icon-holder">
            <i class="c-blue-500 ti-layout"></i>
        </span>
        <span class="title">Content Management</span>
    </a>
</li>
<li class="nav-item active">
    <a class='sidebar-link' href="{{ route(ADMIN . '.users.index') }}">
        <span class="icon-holder">
            <i class="c-blue-500 ti-user"></i>
        </span>
        <span class="title">Users</span>
    </a>
</li>

