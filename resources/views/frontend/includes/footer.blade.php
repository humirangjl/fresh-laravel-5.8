<footer class="bg-common onsite">
    <img class="texture inverted {{ Request::segment(1) == '' || Request::segment(1) == 'gift-card' ? '' : 'dark-theme' }}" src="{{ asset('frontend') }}/img/texture.png" alt="">
    <div class="container container-wide">
        <div class="row">
            <div class="col-lg-3 col-md-6 footer-links">
                <h2>{{ $hf->section_first[0]->content }}</h2>
                <ul class="list-unstyled">
                    <li><a href="{{ url('/#rooms-section') }}" title="">{{ $hf->section_first[1]->content }}</a></li>
                    <li><a href="{{ url('/#booking-section') }}" title="">{{ $hf->section_first[2]->content }}</a></li>
                    <li><a href="{{ url('/team-building') }}" title="">{{ $hf->section_first[3]->content }}</a></li>
                    <li><a href="{{ url('/gift-card') }}" title="">{{ $hf->section_first[4]->content }}</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-6 footer-links">
                <h2>{{ $hf->section_second[0]->content }}</h2>
                <ul class="list-unstyled">
                    <li><a href="{{ url('/faq') }}" title="">{{ $hf->section_second[1]->content }}</a></li>
                    <li><a href="{{ url('/contact-us') }}" title="">{{ $hf->section_second[2]->content }}</a></li>
                    <li><a href="{{ url('/work-with-us') }}" title="">{{ $hf->section_second[3]->content }}</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-6 social-links">
                <h2>{{ $hf->section_third[0]->content }}</h2>
                <ul class="list-inlined">
                    <li class="list-inline-item"><a href="https://www.facebook.com/{{ $social['facebook'] }}" class="fa fa-facebook"></a></li>
                    <li class="list-inline-item"><a href="https://www.instagram.com/{{ $social['instagram'] }}" class="fa fa-instagram"></a></li>
                    <li class="list-inline-item"><a href="https://www.twitter.com/{{ $social['twitter'] }}" class="fa fa-twitter"></a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-6 address">
                <img class="logo" src="{{ asset('uploads/logo/'.$configuration->logo) }}" alt="">
                <ul class="list-unstyled">
                    <li>{{ $hf->section_fourth[0]->content }}: {{ $configuration->address }} </li>
                    @foreach($contacts as $contact)
                    <li>{{ $loop->first ? $hf->section_fourth[1]->content.':' : '' }} {{ $contact }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <ul class="list-inlined text-center">
                <li class="list-inline-item">{{ $configuration->copyright }}</li>
                <li class="list-inline-item"><a href="{{ url('/press') }}" title="">{{ $hf->section_sixth[0]->content }}</a></li>
                <li class="list-inline-item"><a href="{{ url('/privacy-policy') }}" title="">{{ $hf->section_sixth[1]->content }}</a></li>
                <li class="list-inline-item"><a href="{{ url('/terms-and-conditions') }}" title="">{{ $hf->section_sixth[2]->content }}</a></li>
            </ul>
        </div>
    </div>
</footer>