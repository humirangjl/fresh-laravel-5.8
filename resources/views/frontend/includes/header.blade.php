<header class="navigation">
    <div class="container-fluid relative">
        <nav class="navbar navbar-expand-lg">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{ asset('uploads/logo/'.$configuration->logo) }}">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse pull-right" id="navbarCollapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                    <a class="nav-link" href="{{ url('/team-building') }}">{{ $hf->section_first[3]->content }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/gift-card') }}">{{ $hf->section_first[4]->content }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/faq') }}">{{ $hf->section_second[1]->content }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/contact-us') }}">{{ $hf->section_second[2]->content }}</a>
                    </li>
                </ul>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item book">
                        <a class="nav-link" href="{{ url('/#booking-section') }}" id="book-now-header">{{ $hf->section_fifth[0]->content }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link language language-ca" data-lang="ca"><img src="{{ asset('frontend') }}/img/country1.png" alt=""></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link language language-sp" data-lang="sp"><img src="{{ asset('frontend') }}/img/country2.png" alt=""></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link language language-en" data-lang="en"><img src="{{ asset('frontend') }}/img/country3.png" alt=""></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link language language-fr" data-lang="fr"><img src="{{ asset('frontend') }}/img/country4.png" alt=""></a>
                    </li>
                    <li class="nav-item social spacer-social">
                        <a class="nav-link" href="https://www.facebook.com/{{ $social['facebook'] }}"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li class="nav-item social">
                        <a class="nav-link" href="https://www.instagram.com/{{ $social['instagram'] }}"><i class="fa fa-instagram"></i></a>
                    </li>
                    <li class="nav-item social">
                        <a class="nav-link" href="https://www.twitter.com/{{ $social['twitter'] }}"><i class="fa fa-twitter"></i></a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</header>
