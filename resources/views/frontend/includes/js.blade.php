<script src="{{ asset('frontend') }}/js/vendor/jquery-v3.3.1.min.js"></script>
<script src="{{ asset('frontend') }}/js/vendor/popper.min.js"></script>
<script src="{{ asset('frontend') }}/js/vendor/bootstrap.min.js"></script>
<script src="{{ asset('frontend') }}/js/main.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="{{ asset('frontend') }}/js/owl.carousel.min.js"></script>
<script src="{{ asset('frontend') }}/js/flicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setTimeout(
            function(){
                $('#loadingDiv').hide();
            }, 800);
    });

    $(document).ready(function () {
        $(document).on('click','.language-ca',function(e){
            var lang = $(this).data('lang');
            var data = {
                lang: lang
            };
            $.get("{{ url('change-language-ca') }}", data,
                function (data, textStatus, jqXHR) {
                    if(data.result == 'success'){
                        location.href = 'http://ca.centropenitenciariodelclot.com';
                    }
                },
                "json"
            );
        });
    });
    $(document).ready(function () {
        $(document).on('click','.language-sp',function(e){
            var lang = $(this).data('lang');
            var data = {
                lang: lang
            };
            $.get("{{ url('change-language-sp') }}", data,
                function (data, textStatus, jqXHR) {
                    if(data.result == 'success'){
                        location.href = 'http://centropenitenciariodelclot.com';
                    }
                },
                "json"
            );
        });
    });
    $(document).ready(function () {
        $(document).on('click','.language-fr',function(e){
            var lang = $(this).data('lang');
            var data = {
                lang: lang
            };
            $.get("{{ url('change-language-fr') }}", data,
                function (data, textStatus, jqXHR) {
                    if(data.result == 'success'){
                        location.href = 'http://fr.centropenitenciariodelclot.com';
                    }
                },
                "json"
            );
        });
    });
    $(document).ready(function () {
        $(document).on('click','.language-en',function(e){
            var lang = $(this).data('lang');
            var data = {
                lang: lang
            };
            $.get("{{ url('change-language-en') }}", data,
                function (data, textStatus, jqXHR) {
                    if(data.result == 'success'){
                        location.href = 'http://en.centropenitenciariodelclot.com';
                    }
                },
                "json"
            );
        });
    });

    $("#cf").hover(function () {
        $(".banner-content").css("visibility", "hidden");
    }, function () {
        $(".banner-content").css("visibility", "visible");
    });

    var url = $('input[name=url]').val();
    if(url == "" || url == "ca" || url == "fr" || url == "en"){
        $(document).ready(function () {
            $('.section-banner').css({
                'min-height': $(window).height(),
                'padding-top': $('header').height() + 5
            });
        });
        $(window).resize(function () {
            $('.section-banner').css({
                'min-height': $(window).height(),
                'padding-top': $('header').height() + 5
            });
        });
    }
</script>
<script>
$(document).on('click','#booking-section',function(e){
    e.preventDefault();
    var element = document.getElementById("booking-section");
     element.scrollIntoView();
});
</script>
<script>
$(document).on('click','#rooms-section',function(e){
    e.preventDefault();
    var element = document.getElementById("rooms-section");
     element.scrollIntoView();
});
</script>
@yield('js')
