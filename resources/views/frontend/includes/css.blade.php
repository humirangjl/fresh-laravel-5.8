<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Teko:400,700" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('frontend') }}/css/bootstrap.min.css">
<link rel="icon" href="{{ asset('frontend') }}/css/favicon.ico">
<link rel="stylesheet" href="{{ asset('frontend') }}/css/font-awesome.min.css">
<link rel="stylesheet" href="{{ asset('frontend') }}/css/main.css">
<link rel="stylesheet" href="{{ asset('frontend') }}/css/owl.carousel.min.css">
<link rel="stylesheet" href="{{ asset('frontend') }}/css/responsive.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" />
@yield('css')
<style>
.language {
    cursor:pointer;
}
.nice-select .current {
    width:100%;
}
.mod-close{
    cursor: pointer;
}
</style>