<?php

namespace App;

use DB;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Models\Configuration;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;
use Session, Auth, File, Image, Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'avatar', 'bio', 'role', 'token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /*
    |------------------------------------------------------------------------------------
    | Validations
    |------------------------------------------------------------------------------------
    */
    public static function rules($update = false, $id = null)
    {
        $commun = [
            'email'    => "required|email|unique:users,email,$id",
            'password' => 'nullable|confirmed',
            'avatar' => 'image',
        ];

        if ($update) {
            return $commun;
        }

        return array_merge($commun, [
            'email'    => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /*
    |------------------------------------------------------------------------------------
    | Backend Update
    |------------------------------------------------------------------------------------
    */
    public static function changeCredentials($request)
    {
        $admin = Auth::user();
        if($admin->role == 10){
            $admin->email = $request->new_email;
            $admin->save();
            if($request->new_password){
                $admin->password = $request->new_password;
                $admin->save();
            } return true;
        } return false;
    }

    /*
    |------------------------------------------------------------------------------------
    | Attributes
    |------------------------------------------------------------------------------------
    */
    public function setPasswordAttribute($value='')
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function getAvatarAttribute($value)
    {
        if (!$value) {
            return 'http://placehold.it/160x160';
        }

        return '/uploads/avatar/'.Auth::user()->id.'/'.rawurlencode($value);
    }
    public function setAvatarAttribute($photo)
    {
        $file = Image::make($photo);
        $generated_filename = $photo->getClientOriginalName();

        $this->attributes['avatar'] = $generated_filename;
    }

    /*
    |------------------------------------------------------------------------------------
    | Boot
    |------------------------------------------------------------------------------------
    */
    public static function boot()
    {
        parent::boot();
        static::updating(function($user)
        {
            $original = $user->getOriginal();

            if (\Hash::check('', $user->password)) {
                $user->attributes['password'] = $original['password'];
            }
        });
    }

    public static function sendVerification($request_email)
    {
        $configuration = Configuration::find(1);
        $email         = $configuration->email;
        $token         = Crypt::encrypt($request_email);
        $to            = $request_email;
        // Send mail confirmation
        Mail::send(
            'emails.registration',
            array('token' => $token),
            function($message) use($email, $to)
            {
                $message->to($to);
                $message->subject('Account Confirmation');
                $message->from($email, 'Sploit Security');
        });
    }

    public static function forgotPassword($request){
        // get data
        $data = self::where('email', $request->email)->where('id','<>',1)->first();
        // Check if data exist
        if($data){
            // get admin data
            $configuration = Configuration::find(1);    
            $token = $data->id;
            DB::table('password_resets')->insert([
                'email'      => $request->email,
                'token'      => $token,
                'created_at' => Carbon::now()
            ]);    
            // Send password reset link.
            Mail::send('emails.forgot-password', array('token' => $token, 'user' => $data, 'configuration' => $configuration),
                function($message) use ($data, $configuration){
                $message->to($data->email)->subject($configuration->name.': Forgot Password');
                $message->from($configuration->email, $configuration->name.' Management');
            });
            return true;
        }
        return false;
    }
    public static function resetPassword($request)
    {
        $data = self::find(Crypt::decrypt($request->id));
        if($data){
            $data->password = $request->new_password;

            $data->save();
            return true;
        }
        return false;
    }
}
