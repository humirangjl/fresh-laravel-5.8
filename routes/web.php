<?php
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

/*
|------------------------------------------------------------------------------------
| Admin
|------------------------------------------------------------------------------------
*/
Route::group(['namespace' => 'Backend', 'prefix' => ADMIN, 'as' => ADMIN . '.', 'middleware'=>['auth', 'Role:10']], function () {
    Route::get('/', 'MainController@getIndex');
    Route::resource('users',              'UserController');
    Route::group(["prefix" => "content-management"], function(){
        Route::get('/remove-repeater-fields', 'ContentManagementController@getRemoveRepeaterFields');
        Route::get('/repeater-fields'       , 'ContentManagementController@getRepeaterFields');
    });
    Route::resource('content-management', 'ContentManagementController');
    Route::group(["prefix" => "settings"], function(){
        // ---------- Post Method ---------- //
        Route::post('default-cover',  'SettingsController@postDefaultCover');
        Route::post('default-avatar', 'SettingsController@postDefaultAvatar');
        Route::post('social',         'SettingsController@postSocial');
        Route::post('general',        'SettingsController@postGeneral');
        // ---------- Get Method ---------- //
        Route::get('/',               'SettingsController@getIndex');
    });
});
Route::group(['namespace' => 'Frontend'], function () {
    // POST REQUEST
    // GET REQUEST
    Route::get('/', 'MainController@getIndex');
});
Auth::routes();
